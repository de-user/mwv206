/*
 * JM7200 GPU driver
 *
 * Copyright (c) 2018 ChangSha JingJiaMicro Electronics Co., Ltd.
 *
 * Author:
 *      rfshen <jjwgpu@jingjiamicro.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 */
#ifndef _MWV206PLATFORM_H_
#define _MWV206PLATFORM_H_

#ifdef __cplusplus
extern "C" {
#endif

#define V206PLATFORM001     1
#define V206PLATFORM002  2
#define V206PLATFORM003 3
#define V206PLATFORM004  4
#define V206PLATFORM005   5
#define V206PLATFORM006  6
#define V206PLATFORM007 7
#define V206PLATFORM008    8

#define V206PLATFORM009        (101)
#define V206PLATFORM010    (102)
#define V206PLATFORM011             (103)
#define V206PLATFORM012        (104)
#define V206PLATFORM013        (105)
#define _MWV206_KYLIN_          (106)
#define _MWV206_NEOKYLIN_       (107)
#define V206PLATFORM014           (108)
#define _MWV206_DEEPIN_         (109)

#define _MWV206_FT2000AHK_  (201)
#define _MWV206_LOONGSON_   (202)
#define _MWV206_AARCH64_    (203)
#define _MWV206_T1022_      (204)
#define V206PLATFORM015        (205)

#ifdef __cplusplus
}
#endif

#endif